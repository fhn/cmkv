#ifndef INIT_HH
# define INIT_HH

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <vector>
#include <random>
#include "init.hh"

template<typename T> using matrix = std::vector<std::vector<T>>;

void generateRandom(matrix<int>& m, const cv::Mat3b& img, const int numClass);
void initMatrix(matrix<int>& m, const cv::Mat3b& img);

#endif
