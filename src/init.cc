#include "init.hh"

void initMatrix(matrix<int>& m, const cv::Mat3b& img)
{
    for (int i = 0; i < img.rows; ++i)
        m.push_back(std::vector<int>(img.cols));

}

void generateRandom(matrix<int>& m, const cv::Mat3b& img, const int numClass)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, numClass - 1);
    int j = 0;

    for (int i = 0; i < img.rows; ++i)
        for (j = i % 2 ? 0 : 1 ; j < img.cols; j+=2)
        {
            int r = dis(gen);
            m[i][j] = r == 4 ? r - 1 : r;
        }

    for (int i = 0; i < img.rows; ++i)
        for (j = i % 2 ? 1 : 0 ; j < img.cols; j+=2)
        {
            int r = dis(gen);
            m[i][j] = r == 4 ? r - 1 : r;
        }
}
