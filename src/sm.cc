#include "sm.hh"

float determinant(cv::Mat& mat)
{
    float a = mat.at<float>(0,0);
    float b = mat.at<float>(0,1);
    float c = mat.at<float>(0,2);
    float d = mat.at<float>(1,0);
    float e = mat.at<float>(1,1);
    float f = mat.at<float>(1,2);
    float g = mat.at<float>(2,0);
    float h = mat.at<float>(2,1);
    float i = mat.at<float>(2,2);

    return (a*e*i + d*h*c + g*b*f) - (g*e*c + a*h*f + d*b*i);
}

sm::sm(const cv::Mat3b& src)
: src(src)
{
    initMatrix(labMat,src);
}

cv::Mat sm::covMat(int x, int y)
{
    cv::Vec3b pix = src.at<cv::Vec3b>(x,y);
    float mean = (pix(0) + pix(1) + pix(2)) / 3.0;
    //Rows 3 and cols 1
    cv::Mat pixM(3,1, CV_32FC1);
    pixM.at<float>(0) = pix(0) - mean;
    pixM.at<float>(1) = pix(1) - mean;
    pixM.at<float>(2) = pix(2) - mean;

    cv::Mat covMat(3,3, CV_32FC1);
    cv::mulTransposed(pixM,covMat,false);

    return covMat;
}

void sm::computeMeanAndVarOneRegion(cv::Mat3b& region)
{
    int lastKeyM = 0;

    float M1 = 0;
    float M2 = 0;
    float M3 = 0;
    unsigned int numOfPixels = region.cols * region.rows;
    for (int i = 0; i < region.rows; ++i)
        for (int j = 0; j < region.cols; ++j)
        {
            M1 += region.at<cv::Vec3b>(i,j)[0];
            M2 += region.at<cv::Vec3b>(i,j)[1];
            M3 += region.at<cv::Vec3b>(i,j)[2];
        }
    if (!means.empty())
    {
        lastKeyM = means.rbegin()->first + 1;
    }
    
    M1 = M1 / numOfPixels;
    M2 = M2 / numOfPixels;
    M3 = M3 / numOfPixels;
    means.insert(std::make_pair(lastKeyM, std::make_tuple(M1, M2, M3)));
    cv::Mat m(3,1,CV_32FC1);
    m.at<float>(0,0) = M1;
    m.at<float>(0,1) = M2;
    m.at<float>(0,2) = M3;
    
    cv::Mat s = cv::Mat::zeros(3,3,CV_32FC1);
    cv::Mat tmp = cv::Mat::zeros(3,3,CV_32FC1);
    for (int i = 0; i < region.rows; ++i)
        for (int j = 0; j < region.cols; ++j)
        {
             cv::mulTransposed(vec2Mat(region.at<cv::Vec3b>(i,j)) -
                        m,tmp,false);
             s = s + tmp;
        }
    s = s / (numOfPixels - 1);
    mapCov.insert(std::make_pair(lastKeyM, s));

}

float sm::singleton(int i, int j, int label)
{
    cv::Mat covM; //= covMat(i,j);
    cv::Mat tmp(3,1,CV_32FC1);
    tmp.at<float>(0) = std::get<0>(means[label]);
    tmp.at<float>(1) = std::get<1>(means[label]);
    tmp.at<float>(2) = std::get<2>(means[label]);
    
    cv::Mat tmp1 = vec2Mat(src.at<cv::Vec3b>(i,j)); 
    cv::Mat diff = tmp1 - tmp;

    float detC = cv::determinant(mapCov[label]);
    float lhs = log(sqrt(pow(2*M_PI,3)*detC));

    cv::Mat diffT = diff.t();
   
    cv::Mat tmpRes; 
    if (detC >0) 
        tmpRes = diffT * mapCov[label].inv() * diff;
    else
        tmpRes = diffT * mapCov[label] * diff;
    float rhs = 0.5 * tmpRes.at<float>(0);
    return lhs + rhs;
}

float sm::localEnergy(int i,  int j,  int label)
{
    return singleton(i, j, label) + doubleton(i, j, label);
}

float sm::CalculateEnergy()
{
    double singletons = 0.0;
    double doubletons = 0.0;
    int i =0;
    int j = 0;
    int k = 0;
    for (i = 0; i < src.rows; ++i)
        for (j = 0; j < src.cols; ++j)
        {
            k = labMat[i][j];
            singletons += singleton(i, j, k);
            doubletons += doubleton(i, j, k); 
        }
    return singletons + doubletons / 2;
}


float sm::doubleton(int i, int j, int label)
{
    double energy = 0.0;

    if (i != src.rows-1) // south
    {
        if (label == labMat[i+1][j]) energy -= beta;
        else energy += beta;
    }
    if (j != src.cols-1) // east
    {
        if (label == labMat[i][j+1]) energy -= beta;
        else energy += beta;
    }
    if (i !=0 ) // nord
    {
        if (label == labMat[i-1][j]) energy -= beta;
        else energy += beta;
    }
    if (j != 0) // west
    {
        if (label == labMat[i][j-1]) energy -= beta;
        else energy += beta;
    }
    return energy;

}

cv::Mat sm::vec2Mat(const cv::Vec3b& v)
{
    cv::Mat pixM(3,1, CV_32FC1);
    pixM.at<float>(0,0) = v(0);
    pixM.at<float>(1,0) = v(1);
    pixM.at<float>(2,0) = v(2);

    return pixM;
}

void sm::segmentation()
{
    generateRandom(labMat, src, numReg);

    float sumE = 0;
    float currentE = 0;
    float oldE = CalculateEnergy();

    do
    {
        for (int i = 0; i < src.rows; ++i)
        {
            for (int j = 0; j < src.cols; ++j)
            {
                for (unsigned int r = 0; r < numReg; ++r) 
                {
                    if (r == 4)
                    {
                        cv::Mat3b rV(1,1,16);
                        rV.at<cv::Vec3b>(0,0) = src.at<cv::Vec3b>(i,j);
                        cv::cvtColor(rV,rV,CV_Luv2BGR);
                        if (rV.at<cv::Vec3b>(0,0)[0] <= 5 && rV.at<cv::Vec3b>(0,0)[1] <=5 && rV.at<cv::Vec3b>(0,0)[2] >= 250)
                            labMat[i][j] = 4;
                    }
                    else
                    {
                        int lE = localEnergy(i,j,r);
                        if (localEnergy(i,j,labMat[i][j]) > lE)
                        {
                            labMat[i][j] = r;
                        }
                    }
                }
            }
        }
        currentE = CalculateEnergy();
        sumE = fabs(oldE - currentE);
        oldE = currentE;
    }
    while (sumE > t);
}

void sm::preDefineColors()
{
    //Building rose 0
    //Something black 1
    //Water blue 2
    //Zone yellow 3
    //Something red 4
    //Background white-red 5
    //Background-relief gray 6

    colors.insert(std::make_pair(0,cv::Vec3b(70,89,212))); 
    colors.insert(std::make_pair(1,cv::Vec3b(0,0,0))); 
    colors.insert(std::make_pair(2,cv::Vec3b(255,0,0))); 
    colors.insert(std::make_pair(3,cv::Vec3b(34,168,240))); 
    colors.insert(std::make_pair(4,cv::Vec3b(0,0,255))); 
    colors.insert(std::make_pair(5,cv::Vec3b(129,185,227))); 
    colors.insert(std::make_pair(6,cv::Vec3b(84,120,150))); 
}

void sm::setSrcImg(cv::Mat3b& src)
{
    this->src = src;
    labMat.clear();
    initMatrix(labMat,this->src);
}

cv::Mat3b sm::getOutput()
{
    cv::Mat3b out(src.rows, src.cols, 16);
    for (int i = 0; i < src.rows; ++i)
    {
        for (int j = 0; j < src.cols; ++j)
        {
            out.at<cv::Vec3b>(i,j) = colors[labMat[i][j]]; 
        }
    }
    return out;
}


