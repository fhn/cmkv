#include <chrono>
#include <iostream>
#include "sm.hh"

void predefine(sm& obj)
{
    
    obj.setNumReg(7);
    obj.getRegion<std::string>("train/region1.png");
    obj.getRegion<std::string>("train/region2.png");
    obj.getRegion<std::string>("train/region3.png");
    obj.getRegion<std::string>("train/region4.png");
    obj.getRegion<std::string>("train/region5.png");
    obj.getRegion<std::string>("train/region6.png");
    obj.getRegion<std::string>("train/region7.png");
       
    obj.preDefineColors();
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "Usage : ./mrf input.img output.img" << std::endl;
        return 1;
    }
    const std::chrono::time_point<std::chrono::system_clock> start
        = std::chrono::system_clock::now();
    cv::Mat3b img = cv::imread(argv[1], CV_LOAD_IMAGE_COLOR);
    cv::cvtColor(img,img,CV_BGR2Luv);

    sm op(img);
    predefine(op);
    op.segmentation();
    cv::Mat3b dst = op.getOutput();
    cv::imwrite(argv[2],dst);

    const std::chrono::time_point<std::chrono::system_clock> end
        = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    std::cout << "Execution time : " << elapsed_seconds.count()
        << " sec." << std::endl;

    return 0;
}
