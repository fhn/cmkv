#ifndef SM_HH
# define SM_HH

#include <limits>
#include <tuple>
#include <string>
#include <map>
#include <cmath>
#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "init.hh"
#include "fastonebigheader.h"

template<typename T1, typename T2, typename T3>
using v3 = std::tuple<T1,T2,T3>;

class sm
{
    public:
        sm(){};
        sm(const cv::Mat3b& src);
        void preDefineColors();
        void getRegion(cv::Mat3b& region);
        void getRegion(const char *str);
        template<typename T>
            void getRegion(T&& source)
            {
                cv::Mat3b region = cv::imread(std::forward<T>(source), CV_LOAD_IMAGE_COLOR);
                cv::cvtColor(region,region, CV_BGR2Luv);
                computeMeanAndVarOneRegion(region);
            }
        void permute();
        void segmentation();
        void seg();
        void setSrcImg(cv::Mat3b& src);
        void setNumReg(unsigned int i) {numReg = i;};
        cv::Mat3b getOutput();
    private:
        unsigned int numReg = 0;
        float beta = 2;
        float t = 0.05;
        matrix<int> labMat;
        std::map<int, v3<float,float,float>> means;
        std::map<int, cv::Vec3b> colors;
        std::map<int, v3<float,float,float>> variances;
        std::map<int, cv::Mat> mapCov;
        cv::Mat3b src;
        cv::Mat covMat(int x, int y);
        float singleton(int i, int j, int label);
        float doubleton(int i, int j, int label);
        cv::Mat vec2Mat(const cv::Vec3b& v);
        float localEnergy(int i, int j, int label);
        float CalculateEnergy();
        void computeMeanAndVarOneRegion(cv::Mat3b& region);

};

#endif
